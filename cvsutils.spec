Summary: CVS Utilities
Name: cvsutils
Version: 0.2.5
Release: 1
License: GPL
Group: Development/Tools
URL: http://www.red-bean.com/cvsutils/
Source: http://www.red-bean.com/cvsutils/cvsutils-0.2.5.tar.gz
Requires: cvs
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-root

%description
CVS Utilities is a set of scripts that operate on working directories of
CVS (Concurrent Versions System).  Connecting to the CVS server is
avoided whenever possible, which allows certain operations to be
performed offline.  CVS Utilities allows to identify (and discard if
needed) locally modified files and files unknown to CVS, to change CVS
Root and to simulate some CVS commands while offline.

%prep
%setup -q

%build
%configure
make

%install
rm -rf ${RPM_BUILD_ROOT}

%makeinstall

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/*
%doc NEWS README
